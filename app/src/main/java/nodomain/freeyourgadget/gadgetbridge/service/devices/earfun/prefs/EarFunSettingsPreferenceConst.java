package nodomain.freeyourgadget.gadgetbridge.service.devices.earfun.prefs;

public class EarFunSettingsPreferenceConst {
    public static final String PREF_EARFUN_DEVICE_NAME = "pref_earfun_device_name";
    public static final String PREF_EARFUN_AMBIENT_SOUND_CONTROL = "pref_earfun_ambient_sound_control";
    public static final String PREF_EARFUN_TRANSPARENCY_MODE = "pref_earfun_transparency_mode";
    public static final String PREF_EARFUN_ANC_MODE = "pref_earfun_anc_mode";
    public static final String PREF_EARFUN_SINGLE_TAP_LEFT_ACTION = "pref_earfun_single_tap_left_action";
    public static final String PREF_EARFUN_SINGLE_TAP_RIGHT_ACTION = "pref_earfun_single_tap_right_action";
    public static final String PREF_EARFUN_DOUBLE_TAP_LEFT_ACTION = "pref_earfun_double_tap_left_action";
    public static final String PREF_EARFUN_DOUBLE_TAP_RIGHT_ACTION = "pref_earfun_double_tap_right_action";
    public static final String PREF_EARFUN_TRIPPLE_TAP_LEFT_ACTION = "pref_earfun_tripple_tap_left_action";
    public static final String PREF_EARFUN_TRIPPLE_TAP_RIGHT_ACTION = "pref_earfun_tripple_tap_right_action";
    public static final String PREF_EARFUN_LONG_TAP_LEFT_ACTION = "pref_earfun_long_tap_left_action";
    public static final String PREF_EARFUN_LONG_TAP_RIGHT_ACTION = "pref_earfun_long_tap_right_action";
    public static final String PREF_EARFUN_GAME_MODE = "pref_earfun_game_mode";
    public static final String PREF_EARFUN_EQUALIZER_BAND_31_5 = "pref_earfun_equalizer_band_31.5";
    public static final String PREF_EARFUN_EQUALIZER_BAND_63 = "pref_earfun_equalizer_band_63";
    public static final String PREF_EARFUN_EQUALIZER_BAND_125 = "pref_earfun_equalizer_band_125";
    public static final String PREF_EARFUN_EQUALIZER_BAND_180 = "pref_earfun_equalizer_band_180";
    public static final String PREF_EARFUN_EQUALIZER_BAND_250 = "pref_earfun_equalizer_band_250";
    public static final String PREF_EARFUN_EQUALIZER_BAND_500 = "pref_earfun_equalizer_band_500";
    public static final String PREF_EARFUN_EQUALIZER_BAND_1000 = "pref_earfun_equalizer_band_1000";
    public static final String PREF_EARFUN_EQUALIZER_BAND_2000 = "pref_earfun_equalizer_band_2000";
    public static final String PREF_EARFUN_EQUALIZER_BAND_4000 = "pref_earfun_equalizer_band_4000";
    public static final String PREF_EARFUN_EQUALIZER_BAND_8000 = "pref_earfun_equalizer_band_8000";
    public static final String PREF_EARFUN_EQUALIZER_BAND_15000 = "pref_earfun_equalizer_band_15000";
    public static final String PREF_EARFUN_EQUALIZER_BAND_16000 = "pref_earfun_equalizer_band_16000";
    public static final String PREF_EARFUN_EQUALIZER_PRESET = "pref_earfun_equalizer_preset";
}
