package nodomain.freeyourgadget.gadgetbridge.service.devices.garmin.fit.exception;

public class FitParseException extends Exception {
    public FitParseException(final String message) {
        super(message);
    }
}
